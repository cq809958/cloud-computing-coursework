#!/usr/bin/env python

import sys

airport = ""
current_airport = ""
current_count = 0
# Read the mapper output as reducer input
for line in sys.stdin:
    airport, count = line.split('\t', 1)
    # count should be type int
    try:
        count = int(count)
    except ValueError:
        # if not, drop this line
        continue
    # If airport not change, count number add 1
    if current_airport == airport:
        current_count += count
    # If airport name is new, write the airport name and start a new count
    else:
        if current_airport:
            # Write result to STDOUT
            print('%s\t%d') % (current_airport, current_count)

        current_count = count
        current_airport = airport

# Output airport name and its count
if current_airport == airport:
    print('%s\t%d') % (current_airport, current_count)