from common import map_func, reduce_func_tuple as reduce_func, shuffle
import multiprocessing as mp
import pandas as pd


def loading_file(filename): # Loading files
	fd = pd.read_csv(filename, header = None)
	df = pd.DataFrame(fd)
	return df

map_in =[]
# Traversing the passenger code column
for row in loading_file('FlightData.csv')[0]:
	map_in.append(row)

# Using multi-threading to running MapReduce-like
if __name__ == '__main__':
	with mp.Pool(processes=mp.cpu_count()) as pool:
		# Using map_func and map_in as input, chunksize is length of map_in divide number of cpu
		# Because my computer has 16 cores, 6 divide 16 is float not integer and chunksize just accept integer, so I set
		# a fixed value in chunksize

		# pool.map function will use the first argument as function and second argumeent as data, each data will put into
		# the function and get the result.
		map_out = pool.map(map_func, map_in, chunksize= 16)
		print(map_out)
		reduce_in = shuffle(map_out)
		print(reduce_in)
		reduce_out = pool.map(reduce_func, reduce_in.items(), chunksize= 16)
		print(reduce_out)

		# Find the passenger had the highest number of flights
		maxN = 0
		for i in range(len(reduce_out)):
			if reduce_out[i][1] > maxN:
				maxN = reduce_out[i][1]
				result = (reduce_out[i][0], maxN)
		print(result)



