#!/usr/bin/env python

import sys
import re

for line in sys.stdin:
    # Original data is separate by ','
    cols = line.split(',')
    # If flight number match number and word format:
    if re.match('^[A-Za-z0-9]+$', cols[1]):
        # print airport name and 1
        print('%s\t%d') % (cols[2], 1)
