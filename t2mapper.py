#!/usr/bin/env python

import sys
import re

for line in sys.stdin:
    # Original data is separate by ','
    cols = line.split(',')
    # If passenger name match number and word format:
    if re.match('^[A-Za-z0-9]+$', cols[0]):
        # print passenger name and 1
        print('%s\t%d') % (cols[0], 1)
