#!/usr/bin/env python

import sys

passenger_name = ''
current_passenger = ''
passenger_name_max = ''
current_count = 0
current_count_max = 0
# Read the mapper output as reducer input
for line in sys.stdin:
    passenger_name, count = line.split('\t', 1)
    # Count should be type int
    try:
        count = int(count)
    # If not, drop this line
    except ValueError:
        continue
    # If passenger name is current passenger name, count add 1
    if current_passenger == passenger_name:
        current_count += count
        # If current count bigger than count max, then let passenger name equal to passenger name max, also current
        # count.
        if current_count > current_count_max:
            current_count_max = current_count
            passenger_name_max = passenger_name
    else:
        # If passenger name is new, write a new name and start count
        if current_passenger:
            print('%s\t%d') % (current_passenger, current_count)

        current_count = count
        current_passenger = passenger_name

# Output the passenger name had the highest number of flights
if current_passenger == passenger_name:
    print('%s\t%d') % (passenger_name_max, current_count_max)
