def map_func(x): # Count there is one x
	""" Simple occurrence mapper """
	return (x, 1)


def shuffle(mapper_out): # Put the same key's value together
	""" Organise the mapped values by key """
	data = {}
	for k, v in mapper_out:
		if k not in data: 
			data[k] = [v]
		else:
			data[k].append(v)
	return data


def reduce_func(x,y):
	""" Simple sum reducer, 2 args """
	return x+y

def reduce_func_tuple(z): # Getting the total number of 1
	""" Simple sum reducer, 1 tuple arg """
	k,v=z
	return (k, sum(v))
